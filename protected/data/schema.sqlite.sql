CREATE TABLE ad (
    ad_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    headline varchar(255),
	text varchar(255),
	description varchar(255),
	image text,
	learn_more INTEGER,
	username varchar(255),
	created datetime
);