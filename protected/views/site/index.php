<div class="panel panel-default">
	<div class="panel-body">

		<div class="row">
			<div class="col-md-4">
				<form id="ad" method="post" action="/site/saveAd">
					<div class="list-group">
						<div class="list-group-item text-center">
							<div class="form-group">
								<button type="button" class="btn btn-primary" onclick="openKCFinder_singleFile()">Find or upload an image</button>
								<input type="hidden" name="image" id="image" value="<?php echo CHtml::encode($data['image']); ?>">
								<img src="" class="img-responsive" id="image1" style="display:none">
							</div>
						</div>

						<div class="list-group-item">
							<div class="form-group">
								<label for="exampleInputEmail1">Text</label>
								<textarea maxlength="90" name="text" class="form-control" rows="3"><?php echo CHtml::encode($data['text']); ?></textarea>
							</div>
						</div>
						
						<div class="list-group-item">
							<div class="form-group">
								<label for="labelForHeadline">Headline</label>
								<input maxlength="25" name="headline" type="text" class="form-control" id="labelForHeadline" value="<?php echo CHtml::encode($data['headline']); ?>">
							</div>
						</div>

						<div class="list-group-item">
							<div class="form-group">
								<label for="exampleInputEmail1">News feed link description</label>
								<textarea maxlength="200" name="description" class="form-control" rows="3"><?php echo CHtml::encode($data['description']); ?></textarea>
							</div>
						</div>
						
						<div class="list-group-item">
							<div class="form-group checkbox">
								<label>
									<input type="checkbox" name="learn_more" onchange="preview()">
									Learn More
								</label>
							</div>
						</div>
						
						<div class="list-group-item">
							<div class="form-group">
								<label for="labelForUsername">Username</label>
								<input maxlength="25" name="username" type="text" class="form-control" id="labelForUsername" value="<?php echo CHtml::encode($data['username']); ?>">
							</div>
						</div>
						
						<div class="list-group-item text-right">
							<div class="form-group">
								<button type="submit" class="btn btn-primary">Clip</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="col-md-8" id="preview">
				<?php $this->renderPartial('preview'); ?>
			</div>
		</div>
	</div>
</div>