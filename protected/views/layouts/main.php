<!DOCTYPE html>
<html>
	<head>
		<title>TrackTheGame | TrackTheGame</title>

		<link href="/css/bootstrap.css" rel="stylesheet" type="text/css"/>
		<link href="/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
		<link href="/css/style.css" rel="stylesheet" type="text/css"/>
		
		<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
		<script src="/js/bootstrap.min.js" type="text/javascript"></script>
	</head>

	<body>
		<div class="container">
			<?php echo $content; ?>
		</div>

		<script type="text/javascript">
			function openKCFinder_singleFile() {
				window.KCFinder = {};
				window.KCFinder.callBack = function(url) {
					$("#image").val(url);
					preview();
					
					// Actions with url parameter here
					window.KCFinder = null;
				};
				//window.open('/kcfinder/browse.php', 'kcfinder_single');
				window.open('/kcfinder/browse.php?type=images', 'kcfinder_single', 'width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');
			}

			function openKCFinder_multipleFiles() {
				window.KCFinder = {};
				window.KCFinder.callBackMultiple = function(files) {
					for (var i; i < files.length; i++) {
						// Actions with files[i] here
					}
					window.KCFinder = null;
				};
				window.open('/kcfinder/browse.php', 'kcfinder_multiple');
			}
			
			function preview()
			{
				var form = $("#ad");
				$.post("/site/preview", form.serialize() + "&tab="+$("#preview_tab .active").data('tab'), function(data) {
					$("#preview").html(data);
				});
			}
			
			$(function() {
				$("#ad input, #ad textarea").change(function() { preview(); });
			});
		</script>
	</body>
</html>